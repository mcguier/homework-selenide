# Overview
Sample test automation project built with: 

 - Java
 - RestAssured, for API tests
 - Selenide, for Web tests
 - JUnit
 - Allure

# Running tests
**Prerequisites:** Java 11 & Maven 3.8.X are required.

To run the whole suite please execute:
```
mvn clean test
```
There are a couple of configs for API & UI tests respectively: 
 
 - [api_config.properties](src/main/resources/api_config.properties)
 - [web_config.properties](src/main/resources/web_config.properties)

Additionally, you can specify explicitly which base URI to use for API tests with param `api.base` in a command line:
```
mvn clean test -Dapi.base=<api-base-uri>
```

# Reporting
Framework utilizes [Allure Report](https://docs.qameta.io/allure-report/) as a main reporting tool. 
To create a report after tests execution simply run from project root folder:
```
mvn allure:serve
```
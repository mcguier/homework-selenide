package models;

import java.util.List;

public class SearchResults {
    public String cursorToken;
    public int originalStatementCount;
    public List<Statement> statements;
}

package models;

import java.util.List;

public class SearchResponse {
    public List<String> topics;
    public SearchResults searchResults;
}

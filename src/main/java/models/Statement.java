package models;

import java.util.List;

public class Statement {
    public String accessionNumber; //":"PR-386ea743f2a90399fb0e4300ddf37d0697abc743",
    public List<String> collapsedStatements; //":["fse1"],
    public String content; //:"TITLE HIT",
    public Boolean context;//":false,
    public int page; //:0,
    public Boolean recurring;//:false,
    public int snippetCount;//:1,
    public int snippetOffset; //":0,
    public String statementId; //:"fse1",
    public int statementIndex;//":1,
    public int statementIndexOffset;//":0
}

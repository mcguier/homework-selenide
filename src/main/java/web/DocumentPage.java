package web;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.hamcrest.MatcherAssert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.core.StringContains.containsString;

public class DocumentPage {
    public SelenideElement doc = $("#mainWrapper .xn-content");

    @Step("Validate that found statement is highlighted in a document")
    public void foundStatementIsHighlighted(String statementId, String statementText) {
        doc.shouldBe(visible);
        ElementsCollection highlighted_blocks =
                $$(String.format("#page1 span[id^=%s][class~='x-grid3-row-blue']", statementId));
        StringBuilder actual = new StringBuilder();
        for (SelenideElement block : highlighted_blocks) {
            actual.append(block.getOwnText());
        }
        MatcherAssert.assertThat("Highlighted string should contain statement.",
                actual.toString(), containsString(statementText));
    }
}
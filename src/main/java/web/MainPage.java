package web;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NotFoundException;

import java.net.URI;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static web.BaseWebTest.typeText;

public class MainPage {
    public SelenideElement searchField = $(".CodeMirror-code");
    public SelenideElement searchFieldText = searchField.find("pre > span");
    public SelenideElement searchResultsContainer = $(".ReactVirtualized__Grid__innerScrollContainer");
    public ElementsCollection searchResultsStatements =
            $$(".ReactVirtualized__Grid__innerScrollContainer > div");

    @Step("Extract statement id from found statement")
    public static String getStatementId(SelenideElement statement) {
        String href = getStatementHref(statement);
        if (href == null) {
            throw new NotFoundException(String.format("Attribute 'data-clipboard-text' was not found in:\n %s",
                    statement.find(".snippetItem__controls button[class~='snippet-copy-button']")));
        }
        String[] params = URI.create(href).getQuery().split("&");
        for (String param : params) {
            if (param.startsWith("hl=")) {
                return param.substring(3);
            }
        }
        throw new NotFoundException(String.format("Statement id was not found in a string:\n %s", href));
    }

    @Step("Get unique link of the given statement")
    private static String getStatementHref(SelenideElement statement) {
        return statement.find(".snippetItem__controls button[class~='snippet-copy-button']")
                .getAttribute("data-clipboard-text");
    }

    @Step("Get text from found statement")
    public static String getStatementText(SelenideElement statement) {
        return statement.find(".snippetItem__content > span").getText();
    }

    @Step("Scroll to the last found statement and return it")
    public SelenideElement getLastStatement() {
        String href = getStatementHref(searchResultsStatements.last());
        while (true) {
            searchResultsStatements.last().scrollIntoView(true);
            String newHref = getStatementHref(searchResultsStatements.last());
            if (href.equals(newHref)) {
                return searchResultsStatements.last();
            }
            href = newHref;
        }
    }

    @Step("Search in a document by the keyword '{keyword}'")
    public void searchWithKeyword(String keyword) {
        this.searchField.shouldBe(visible).click();
        typeText(keyword);
        this.searchFieldText.shouldHave(text(keyword));
        typeText(Keys.ENTER);
        this.searchResultsContainer.shouldBe(visible);
    }

    @Step("Open main page")
    public void open() {
        Selenide.open("/doc/PR-386ea743f2a90399fb0e4300ddf37d0697abc743");
    }
}

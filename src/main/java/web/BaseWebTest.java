package web;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.Properties;

public class BaseWebTest {
    public BaseWebTest() {
        Properties props = new Properties();
        try {
            props.load(getClass().getClassLoader().getResourceAsStream("web_config.properties"));
            Configuration.baseUrl = props.getProperty("baseUrl");
            Configuration.browser = props.getProperty("browser", "chrome");
            Configuration.headless = Boolean.parseBoolean(props.getProperty("headless"));
            Configuration.timeout = Long.parseLong(props.getProperty("timeout"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
    }

    @Step("Type text '{text}'")
    public static void typeText(CharSequence text) {
        Actions actionProvider = new Actions(WebDriverRunner.getWebDriver());
        Action writeText = actionProvider.sendKeys(text).build();
        writeText.perform();
    }

    @AfterAll
    public static void tearDown() {
        Selenide.closeWindow();
    }
}

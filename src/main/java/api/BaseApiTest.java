package api;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;
import java.util.Properties;

public class BaseApiTest {
    private static final String URI_TEMPLATE = "%s://%s";
    private static final String API_BASE_PARAM_NAME = "api.base";
    public RequestSpecification REQUEST;

    public BaseApiTest() {
        Properties props = new Properties();
        try {
            props.load(getClass().getClassLoader().getResourceAsStream("api_config.properties"));
            RestAssured.baseURI = String.format(URI_TEMPLATE,
                    props.getProperty("protocol", "https"),
                    getApiBase(props));
            RestAssured.urlEncodingEnabled = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        REQUEST = RestAssured.given().contentType(ContentType.JSON).filter(new AllureRestAssured());
    }

    private static String getApiBase(Properties props) {
        String commandLineParam = System.getProperty(API_BASE_PARAM_NAME);
        return (commandLineParam != null && !commandLineParam.isEmpty()) ?
                commandLineParam :
                props.getProperty(API_BASE_PARAM_NAME, "rc.alpha-sense.com");
    }
}

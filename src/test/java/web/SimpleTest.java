package web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.codeborne.selenide.Selenide.switchTo;
import static web.MainPage.getStatementId;
import static web.MainPage.getStatementText;

@Feature("Search keyword in a document")
public class SimpleTest extends BaseWebTest {
    @ParameterizedTest(name = "Validate that found statement is highlighted in a document")
    @ValueSource(strings = {"alphasense"})
    @Tag("web")
    @Description("Searches for the given keyword in a document and validates that statement is highlighted")
    public void searchWithKeyword(String keyword) throws InterruptedException {
        MainPage page = new MainPage();
        page.open();
        page.searchWithKeyword(keyword);
        SelenideElement statement = page.getLastStatement();
        statement.click();
        String statementText = getStatementText(statement);
        String statementId = getStatementId(statement);
        switchTo().frame("content-1");
        new DocumentPage().foundStatementIsHighlighted(statementId, statementText);
    }
}
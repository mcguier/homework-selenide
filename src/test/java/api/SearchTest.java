package api;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import models.SearchResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Feature("API Search with a keyword")
public class SearchTest extends BaseApiTest {

    private static final String KEYWORD_SEARCH_ENDPOINT = "/services/i/public-document-data/document/PR-386ea743f2a90399fb0e4300ddf37d0697abc743/keyword-search/?{q}";

    @Test
    @DisplayName("Validate amount of statements")
    @Description("Checks that amount of statements in 'originalStatementCount' equals to size of statements list in a response.")
    @Tag("api")
    public void searchWithKeyword() {
        String params = "keyword=alphasense&slop=15&positiveOnly=false&negativeOnly=false&released=1633003200000";
        SearchResponse response = REQUEST.when()
                .get(KEYWORD_SEARCH_ENDPOINT, params)
                .as(SearchResponse.class);
        assertEquals(response.searchResults.originalStatementCount, response.searchResults.statements.size());
    }

    @Test
    @DisplayName("Validate search results without providing a keyword")
    @Description("Expects response 200 for empty keyword")
    @Tag("api")
    public void searchWithoutKeyword() {
        String params = "keyword=&slop=15&positiveOnly=false&negativeOnly=false&released=1633003200000";
        REQUEST.when()
                .get(KEYWORD_SEARCH_ENDPOINT, params)
                .then()
                .statusCode(200);
    }
}